import { Component } from '@angular/core';

@Component({
  selector: 'footer',
  template: require('./footer.html')
})
export class FooterComponent {};
