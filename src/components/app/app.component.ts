import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Routes, Router } from '@angular/router';

import { HeaderComponent } from '../header';
import { FooterComponent } from '../footer';
import { InboxComponent } from '../inbox';

@Component({
  selector: 'app',
  template: require('./app.html'),
  directives: [ROUTER_DIRECTIVES, HeaderComponent, FooterComponent]
})
@Routes([
  {path: '*', component: InboxComponent}
])
export class AppComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {
    this.router.navigate(['/inbox']);
  }
}
