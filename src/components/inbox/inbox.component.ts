import { Component } from '@angular/core';

@Component({
  selector: 'inbox',
  template: require('./inbox.html')
})
export class InboxComponent {};
