import 'bootstrap/less/bootstrap.less';
import 'font-awesome/less/font-awesome.less';
import 'simple-line-icons/less/simple-line-icons.less';
import './assets/css/modern.min.css'; // @TODO: rewrite to less

import {bootstrap} from '@angular/platform-browser-dynamic';
import { ROUTER_PROVIDERS } from '@angular/router';

import { AppComponent } from './components';

bootstrap(AppComponent, [
  ROUTER_PROVIDERS
]);
